﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OPI3_ {
	public static class Prompt {
		public static string ShowDialog(string text) {
			Form prompt = new Form() {
				ClientSize = new System.Drawing.Size(166, 59),
				FormBorderStyle = FormBorderStyle.Sizable,
				Text = text,
				StartPosition = FormStartPosition.CenterScreen
			};
			Label textLabel = new Label() {AutoSize=true,Location = new System.Drawing.Point(5, 9), Text = text };
			ComboBox cb = new ComboBox() {
				DropDownStyle = ComboBoxStyle.DropDownList,
				FormattingEnabled = true, Location = new System.Drawing.Point(12, 25), Size = new System.Drawing.Size(61, 21)
			};
			cb.Items.AddRange(new object[] { "Ф", "Т", "С", "К" });
			cb.Text = cb.Items.Cast<string>().First();

			Button confirmation = new Button() { Text = "Ok", Location = new System.Drawing.Point(79, 25),
				Size = new System.Drawing.Size(75, 23), DialogResult = DialogResult.OK };
			confirmation.Click += (sender, e) => { prompt.Close(); };
			prompt.Controls.Add(cb);
			prompt.Controls.Add(confirmation);
			prompt.Controls.Add(textLabel);
			prompt.AcceptButton = confirmation;
			prompt.ShowDialog();
			return cb.Text; //prompt.ShowDialog() == DialogResult.OK ? cb.Text;
		}
	}
}
