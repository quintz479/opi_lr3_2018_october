﻿namespace OPI3_ {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.a = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.e = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.g = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.h = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.cbX = new System.Windows.Forms.ComboBox();
            this.cbY = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cbColour = new System.Windows.Forms.ComboBox();
            this.prorah = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.cbXTo = new System.Windows.Forms.ComboBox();
            this.cbYTo = new System.Windows.Forms.ComboBox();
            this.buttonGo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.a,
            this.b,
            this.c,
            this.d,
            this.e,
            this.f,
            this.g,
            this.h});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.MinimumSize = new System.Drawing.Size(0, 30);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 30;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(272, 272);
            this.dataGridView1.TabIndex = 0;
            // 
            // a
            // 
            this.a.HeaderText = "a";
            this.a.Name = "a";
            this.a.ReadOnly = true;
            this.a.Width = 30;
            // 
            // b
            // 
            this.b.HeaderText = "b";
            this.b.Name = "b";
            this.b.ReadOnly = true;
            this.b.Width = 30;
            // 
            // c
            // 
            this.c.HeaderText = "c";
            this.c.Name = "c";
            this.c.ReadOnly = true;
            this.c.Width = 30;
            // 
            // d
            // 
            this.d.HeaderText = "d";
            this.d.Name = "d";
            this.d.ReadOnly = true;
            this.d.Width = 30;
            // 
            // e
            // 
            this.e.HeaderText = "e";
            this.e.Name = "e";
            this.e.ReadOnly = true;
            this.e.Width = 30;
            // 
            // f
            // 
            this.f.HeaderText = "f";
            this.f.Name = "f";
            this.f.ReadOnly = true;
            this.f.Width = 30;
            // 
            // g
            // 
            this.g.HeaderText = "g";
            this.g.Name = "g";
            this.g.ReadOnly = true;
            this.g.Width = 30;
            // 
            // h
            // 
            this.h.HeaderText = "h";
            this.h.Name = "h";
            this.h.ReadOnly = true;
            this.h.Width = 30;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(290, 261);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Формат.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbType
            // 
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Кр",
            "Ф",
            "Т",
            "С",
            "К",
            "п"});
            this.cbType.Location = new System.Drawing.Point(290, 68);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(61, 21);
            this.cbType.TabIndex = 2;
            // 
            // cbX
            // 
            this.cbX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbX.FormattingEnabled = true;
            this.cbX.Items.AddRange(new object[] {
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h"});
            this.cbX.Location = new System.Drawing.Point(290, 12);
            this.cbX.Name = "cbX";
            this.cbX.Size = new System.Drawing.Size(61, 21);
            this.cbX.TabIndex = 2;
            // 
            // cbY
            // 
            this.cbY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbY.FormattingEnabled = true;
            this.cbY.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cbY.Location = new System.Drawing.Point(357, 12);
            this.cbY.Name = "cbY";
            this.cbY.Size = new System.Drawing.Size(61, 21);
            this.cbY.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(424, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Добавити";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(424, 95);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "За замовч.";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cbColour
            // 
            this.cbColour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbColour.FormattingEnabled = true;
            this.cbColour.Items.AddRange(new object[] {
            "Black",
            "White"});
            this.cbColour.Location = new System.Drawing.Point(357, 68);
            this.cbColour.Name = "cbColour";
            this.cbColour.Size = new System.Drawing.Size(61, 21);
            this.cbColour.TabIndex = 2;
            // 
            // prorah
            // 
            this.prorah.Location = new System.Drawing.Point(424, 11);
            this.prorah.Name = "prorah";
            this.prorah.Size = new System.Drawing.Size(82, 23);
            this.prorah.TabIndex = 1;
            this.prorah.Text = "Прорахувати";
            this.prorah.UseVisualStyleBackColor = true;
            this.prorah.Click += new System.EventHandler(this.prorah_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(424, 124);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 23);
            this.button4.TabIndex = 1;
            this.button4.Text = "За замовч.2";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button3___Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(371, 261);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 23);
            this.button5.TabIndex = 1;
            this.button5.Text = "Перевага";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Perevaga);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(424, 66);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(82, 23);
            this.button6.TabIndex = 1;
            this.button6.Text = "Видалити";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.del_Click);
            // 
            // cbXTo
            // 
            this.cbXTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbXTo.FormattingEnabled = true;
            this.cbXTo.Items.AddRange(new object[] {
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h"});
            this.cbXTo.Location = new System.Drawing.Point(290, 39);
            this.cbXTo.Name = "cbXTo";
            this.cbXTo.Size = new System.Drawing.Size(61, 21);
            this.cbXTo.TabIndex = 2;
            this.cbXTo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // cbYTo
            // 
            this.cbYTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbYTo.FormattingEnabled = true;
            this.cbYTo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cbYTo.Location = new System.Drawing.Point(357, 39);
            this.cbYTo.Name = "cbYTo";
            this.cbYTo.Size = new System.Drawing.Size(61, 21);
            this.cbYTo.TabIndex = 2;
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(424, 153);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(82, 23);
            this.buttonGo.TabIndex = 1;
            this.buttonGo.Text = "Йти";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.button_go);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 296);
            this.Controls.Add(this.cbColour);
            this.Controls.Add(this.cbYTo);
            this.Controls.Add(this.cbY);
            this.Controls.Add(this.cbXTo);
            this.Controls.Add(this.cbX);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.prorah);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ComboBox cbType;
		private System.Windows.Forms.ComboBox cbX;
		private System.Windows.Forms.ComboBox cbY;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.ComboBox cbColour;
		private System.Windows.Forms.Button prorah;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.DataGridViewTextBoxColumn a;
		private System.Windows.Forms.DataGridViewTextBoxColumn b;
		private System.Windows.Forms.DataGridViewTextBoxColumn c;
		private System.Windows.Forms.DataGridViewTextBoxColumn d;
		private System.Windows.Forms.DataGridViewTextBoxColumn e;
		private System.Windows.Forms.DataGridViewTextBoxColumn f;
		private System.Windows.Forms.DataGridViewTextBoxColumn g;
		private System.Windows.Forms.DataGridViewTextBoxColumn h;
		private System.Windows.Forms.ComboBox cbXTo;
		private System.Windows.Forms.ComboBox cbYTo;
		private System.Windows.Forms.Button buttonGo;
	}
}

