﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OPI3_ {

	public partial class Form1 : Form {

		private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e) {
			cbX.Text = itox(dataGridView1.CurrentCell.ColumnIndex);
			cbY.Text = itoy(7 - dataGridView1.CurrentCell.RowIndex);
		}
		private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e) { }

		private void dgGrid_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e) {
			var grid = sender as DataGridView;
			var rowIdx = (8 - (e.RowIndex)).ToString();

			var centerFormat = new StringFormat() {
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};

			var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
			e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
		}
		Dictionary<string, string>[,] mat = new Dictionary<string, string>[8, 8]{
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
  { new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>(), new Dictionary<string,string>() },
};

		void InitGr() {
			for (int i = 0; i < 8; i++)
				for (int j = 0; j < 8; j++) {
					mat[i, j].Clear();
					mat[i, j]["Who"] = "";
					mat[i, j]["Colour"] = "";


				}
			for (int i = 0; i < 8; i++) {
				dataGridView1.RowPostPaint += dgGrid_RowPostPaint;
			}
		}
		void DefaultGr() {
			for (int i = 0; i < 8; i++)
				for (int j = 0; j < 8; j++) {
					mat[i, j].Clear();
					mat[i, j]["Who"] = "";
				}
		}

		void Add(int x, int y) {
			mat[x, y].Add("name", "");
		}

		void check_p() {
			for (int i = 0; i < 8; i++) {
				if (mat[i, 0]["Who"] == "п")
					mat[i, 0]["Who"] = Prompt.ShowDialog("Пішак " + itox(i) + itoy(0) + " має перетворитись.");
				if (mat[i, 7]["Who"] == "п")
					mat[i, 7]["Who"] = Prompt.ShowDialog("Пішак " + itox(i) + itoy(7) + " має перетворитись.");
			}
		}

		void UpdateGrid() {
			check_p();
			for (int i = 0; i < 8; i++)
				for (int j = 0; j < 8; j++) {
					var c = dataGridView1.Rows[7 - j].Cells[i];
					c.Value = mat[i, j]["Who"];
					c.Style.ForeColor = (mat[i, j]["Colour"] == "Black") ? Color.Black : Color.White;
					c.Style.BackColor = ColorTranslator.FromHtml((((i + j) % 2) == 0) ? "#FFCE9E" : "#D18B47");
				}
		}
		void setColourGrid(int x, int y) {
			var c = dataGridView1.Rows[7 - y].Cells[x];
			c.Style.BackColor = ColorTranslator.FromHtml("#00FF00");
		}
		void setColourGridFight(int x, int y) {
			var c = dataGridView1.Rows[7 - y].Cells[x];
			c.Style.BackColor = ColorTranslator.FromHtml("#FF0000");
		}
		int xtoi(string s) { return (s.First() - 'a'); }
		string itox(int s) { return "" + ((char)(s + 'a')); }
		int ytoi(string s) { return (Int32.Parse(s) - 1); }
		string itoy(int s) { return (s + 1) + ""; }
		bool testif(int x, int y) { return ((x < 8) && (x >= 0) && (y < 8) && (y >= 0)); }
		bool testif_(int x, int y) { return testif(x, y) && (mat[x, y]["Who"] == ""); }
		string testif_r(int x, int y) {
			if (testif_(x, y)) {
				setColourGrid(x, y);
				return (itox(x) + itoy(y)) + "; ";
			}
			return "";
		}


		private void _data_grid_view_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e) {
			foreach (DataGridViewRow i in dataGridView1.Rows) { i.Height = 30; }
		}
		public Form1() {
			InitializeComponent();
			dataGridView1.RowsAdded += new DataGridViewRowsAddedEventHandler(_data_grid_view_RowsAdded);
			dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
			dataGridView1.CellContentDoubleClick += new DataGridViewCellEventHandler(prorah_ClickSilent);

			cbType.Text = cbType.Items.Cast<string>().First();
			cbX.Text = cbX.Items.Cast<string>().First();
			cbY.Text = cbY.Items.Cast<string>().First();
			cbXTo.Text = cbX.Items.Cast<string>().First();
			cbYTo.Text = cbY.Items.Cast<string>().First();
			cbColour.Text = cbColour.Items.Cast<string>().First();
			dataGridView1.Rows.AddRange(new DataGridViewRow[] { new DataGridViewRow(), new DataGridViewRow(), new DataGridViewRow(), new DataGridViewRow(), new DataGridViewRow(), new DataGridViewRow(), new DataGridViewRow(), new DataGridViewRow() });
			dataGridView1.RowCount = 8;
			InitGr();
			UpdateGrid();
		}

		private void button1_Click(object sender, EventArgs e) {
			//InitGr();
			UpdateGrid();
		}

		private void button2_Click(object sender, EventArgs e) {
			if (mat[xtoi(cbX.Text), ytoi(cbY.Text)]["Who"] == "") {
				mat[xtoi(cbX.Text), ytoi(cbY.Text)]["Who"] = cbType.Text;
				mat[xtoi(cbX.Text), ytoi(cbY.Text)]["Colour"] = cbColour.Text;
				if (cbType.Text == "п") mat[xtoi(cbX.Text), ytoi(cbY.Text)]["FirstRunP"] = "True";
			} else MessageBox.Show("Помилка. Клітинка зайнята.");
			UpdateGrid();
		}


		private void button3_Click(object sender, EventArgs e) {
			InitGr();
			string[] arr = new string[8] { "Т", "К", "С", "Ф", "Кр", "С", "К", "Т" };
			for (int i = 0; i < 8; i++) {
				mat[i, 7]["Who"] = mat[i, 0]["Who"] = arr[i];
				mat[i, 6]["Who"] = mat[i, 1]["Who"] = "п";
				mat[i, 6]["FirstRunP"] = mat[i, 1]["FirstRunP"] = "True";
				mat[i, 6]["Colour"] = mat[i, 7]["Colour"] = "White";
				mat[i, 0]["Colour"] = mat[i, 1]["Colour"] = "Black";
			}
			UpdateGrid();
		}
		private void button3___Click(object sender, EventArgs e) {
			InitGr();
			string[] arr = new string[8] { "Т", "К", "С", "Ф", "Кр", "С", "К", "Т" };
			for (int i = 0; i < 8; i++) {
				mat[i, 7]["Who"] = mat[i, 0]["Who"] = arr[i];
				mat[i, 5]["Who"] = mat[i, 2]["Who"] = arr[i];
				mat[i, 5]["Colour"] = mat[i, 7]["Colour"] = "White";
				mat[i, 2]["Colour"] = mat[i, 0]["Colour"] = "Black";
			}
			UpdateGrid();
		}

		private void prorah_ClickSilent(object sender, EventArgs e) { prorah_Cl(sender, e, false); }
		private void prorah_Click(object sender, EventArgs e) { prorah_Cl(sender, e, false); }

		private void setColourGridForFigure(int x, int y, Func<int, int, bool, List<KeyValuePair<int, int>>> f) {
			f(x, y, false).ForEach(keyvalue => setColourGrid(keyvalue.Key, keyvalue.Value));
		}
		private void setColourGridForFigureFight(int x, int y, Func<int, int, bool, List<KeyValuePair<int, int>>> f) {
			List<KeyValuePair<int, int>> allhody = f(x, y, false);
			List<KeyValuePair<int, int>> allhodyplusFight = f(x, y, true);
			allhodyplusFight.Where(hid => !allhody.Contains(hid)).ToList().ForEach(keyvalue => setColourGridFight(keyvalue.Key, keyvalue.Value));
		}

		private List<KeyValuePair<int, int>> kin(int x, int y, bool fight = false) {
			Func<int, int, bool> t;
			if (fight) t = delegate (int x_, int y_) { return testif(x_, y_) && (mat[x, y]["Colour"] != mat[x_, y_]["Colour"]); }; else t = testif_;

			Action<int, int, List<KeyValuePair<int, int>>> addToList = delegate (int x_, int y_, List<KeyValuePair<int, int>> l_) { if (t(x_, y_)) l_.Add(new KeyValuePair<int, int>(x_, y_)); };

			var aList = new List<KeyValuePair<int, int>>();
			addToList(x + 2, y + 1, aList);
			addToList(x + 2, y - 1, aList);
			addToList(x - 2, y + 1, aList);
			addToList(x - 2, y - 1, aList);
			addToList(x + 1, y + 2, aList);
			addToList(x + 1, y - 2, aList);
			addToList(x - 1, y + 2, aList);
			addToList(x - 1, y - 2, aList);
			return aList;
		}

		private List<KeyValuePair<int, int>> tura(int x, int y, bool fight = false) {
			Func<int, int, bool> t;
			if (fight) t = delegate (int x_, int y_) { return testif(x_, y_) && (mat[x, y]["Colour"] != mat[x_, y_]["Colour"]); }; else t = testif_;

			Action<int, int, List<KeyValuePair<int, int>>> addToList = delegate (int x_, int y_, List<KeyValuePair<int, int>> l_) { if (t(x_, y_)) l_.Add(new KeyValuePair<int, int>(x_, y_)); };

			var aList = new List<KeyValuePair<int, int>>();
			for (int i = 1; i <= 8; i++) { addToList(x + i, y, aList); if (!testif_(x + i, y)) break; }
			for (int i = 1; i <= 8; i++) { addToList(x - i, y, aList); if (!testif_(x - i, y)) break; }
			for (int i = 1; i <= 8; i++) { addToList(x, y + i, aList); if (!testif_(x, y + i)) break; }
			for (int i = 1; i <= 8; i++) { addToList(x, y - i, aList); if (!testif_(x, y - i)) break; }

			return aList;
		}

		private List<KeyValuePair<int, int>> slon(int x, int y, bool fight = false) {
			Func<int, int, bool> t;
			if (fight) t = delegate (int x_, int y_) { return testif(x_, y_) && (mat[x, y]["Colour"] != mat[x_, y_]["Colour"]); }; else t = testif_;

			Action<int, int, List<KeyValuePair<int, int>>> addToList = delegate (int x_, int y_, List<KeyValuePair<int, int>> l_) { if (t(x_, y_)) l_.Add(new KeyValuePair<int, int>(x_, y_)); };

			var aList = new List<KeyValuePair<int, int>>();
			for (int i = 1; i <= 8; i++) { addToList(x + i, y + i, aList); if (!testif_(x + i, y + i)) break; }
			for (int i = 1; i <= 8; i++) { addToList(x + i, y - i, aList); if (!testif_(x + i, y - i)) break; }
			for (int i = 1; i <= 8; i++) { addToList(x - i, y + i, aList); if (!testif_(x - i, y + i)) break; }
			for (int i = 1; i <= 8; i++) { addToList(x - i, y - i, aList); if (!testif_(x - i, y - i)) break; }

			return aList;
		}
		private List<KeyValuePair<int, int>> korol(int x, int y, bool fight = false) {
			Func<int, int, bool> t;
			if (fight) t = delegate (int x_, int y_) { return testif(x_, y_) && (mat[x, y]["Colour"] != mat[x_, y_]["Colour"]); }; else t = testif_;

			Action<int, int, List<KeyValuePair<int, int>>> addToList = delegate (int x_, int y_, List<KeyValuePair<int, int>> l_) { if (t(x_, y_)) l_.Add(new KeyValuePair<int, int>(x_, y_)); };

			var aList = new List<KeyValuePair<int, int>>();
			for (int i = x - 1; i <= x + 1; i++)
				for (int j = y - 1; j <= y + 1; j++)
					addToList(i, j, aList);

			return aList;
		}
		private List<KeyValuePair<int, int>> pishak(int x, int y, bool fight = false) {
			Func<int, int, bool> t;
			//if (fight) t = delegate (int x_, int y_) { return testif(x_, y_) && (mat[x, y]["Colour"] != mat[x_, y_]["Colour"]); }; else t = testif_;

			Action<int, int, List<KeyValuePair<int, int>>> addToList = delegate (int x_, int y_, List<KeyValuePair<int, int>> l_) { if (testif_(x_, y_)) l_.Add(new KeyValuePair<int, int>(x_, y_)); };
			Action<int, int, List<KeyValuePair<int, int>>> addToListFight = delegate (int x_, int y_, List<KeyValuePair<int, int>> l_) { if (testif(x_, y_) &&(mat[x_, y_]["Who"]!="") && (mat[x, y]["Colour"] != mat[x_, y_]["Colour"])) l_.Add(new KeyValuePair<int, int>(x_, y_)); };

			var aList = new List<KeyValuePair<int, int>>();


			bool inverse = mat[x, y]["Colour"] == "White";
			int z = inverse ? -1 : 1;
			bool f = mat[x, y]["FirstRunP"] == "True";
			addToList(x, y + z, aList);
			if ((aList.Count == 1) && f) {
				addToList(x, y + z * 2, aList);
			}
			if (fight) {
				addToListFight(x + z, y + z, aList);
				addToListFight(x - z, y + z, aList);
			}
			return aList;
		}


		private void prorah_Cl(object sender, EventArgs e, bool showMB = false) {

			UpdateGrid();
			void t(int x_, int y_) {
				setColourGridForFigure(x_, y_, tura);
				setColourGridForFigureFight(x_, y_, tura);

			}
			void k(int x_, int y_) {
				setColourGridForFigure(x_, y_, kin);
				setColourGridForFigureFight(x_, y_, kin);
			}
			void c(int x_, int y_) {
				setColourGridForFigure(x_, y_, slon);
				setColourGridForFigureFight(x_, y_, slon);
			}
			void p(int x_, int y_) {
				setColourGridForFigure(x_, y_, pishak);
				setColourGridForFigureFight(x_, y_, pishak);
			}
			void kr(int x_, int y_) {
				setColourGridForFigure(x_, y_, korol);
				setColourGridForFigureFight(x_, y_, korol);
			}
			int x = xtoi(cbX.Text), y = ytoi(cbY.Text);
			if (testif(x, y)) {
				if (mat[x, y]["Who"] != "") {
					string s = "";
					switch (mat[x, y]["Who"]) {
						case "Т": t(x, y); break;
						case "К": k(x, y); break;
						case "С": c(x, y); break;
						case "Ф": t(x, y); c(x, y); break;
						case "Кр":kr(x, y); break;
						case "п": p(x, y); break;
					}
					if (showMB) MessageBox.Show("Фігура " + mat[x, y]["Who"] + " з клітинки " + cbX.Text + cbY.Text + " може ходити на клітинки: " + s);
				} else if (showMB) MessageBox.Show("Пуста клітинка");
			} else if (showMB) MessageBox.Show("Не вірні координати.");
		}

		private void Perevaga(object sender, EventArgs e) {
			int s = 0;
			for (int i = 0; i < 8; i++)
				for (int j = 0; j < 8; j++) {
					int inverse = (mat[i, j]["Colour"] == "White") ? -1 : 1;
					switch (mat[i, j]["Who"]) {
						case "Т": s += 5 * inverse; break;
						case "К": s += 3 * inverse; break;
						case "С": s += 3 * inverse; break;
						case "Ф": s += 10 * inverse; break;
						case "п": s += 1 * inverse; break;
					}
				}
			if (s == 0) MessageBox.Show("Нічия");
			else
				MessageBox.Show("Перевага у " + ((s > 0) ? "Чорних" : "Білих") + " " + Math.Abs(s));
		}

		private void del_Click(object sender, EventArgs e) {

			var i = xtoi(cbX.Text);
			var j = ytoi(cbY.Text);
			mat[i, j].Clear();
			mat[i, j]["Who"] = "";
			mat[i, j]["Colour"] = "";
			UpdateGrid();
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) {

		}

		private void button_go(object sender, EventArgs e) {
			int x = xtoi(cbX.Text), y = ytoi(cbY.Text);
			int xTo = xtoi(cbXTo.Text), yTo = ytoi(cbYTo.Text);
			bool dowork = false;
			switch (mat[x, y]["Who"]) {
				case "К":
					if (kin(x, y, true).Contains(new KeyValuePair<int, int>(xTo, yTo)))	dowork = true; break;
				case "п":
					if (pishak(x, y, true).Contains(new KeyValuePair<int, int>(xTo, yTo))) { 
					dowork = true;
					mat[x, y]["FirstRunP"] = "False";
					}
					break;
				case "Т":
					if (tura(x, y, true).Contains(new KeyValuePair<int, int>(xTo, yTo))) dowork = true; break;
				case "С":
					if (slon(x, y, true).Contains(new KeyValuePair<int, int>(xTo, yTo))) dowork = true;	break;
				case "Ф":
					var l = (slon(x, y, true));l.AddRange(tura(x, y, true));
					if (l.Contains(new KeyValuePair<int, int>(xTo, yTo))) dowork = true; break;
				case "Кр":
					if (korol(x, y, true).Contains(new KeyValuePair<int, int>(xTo, yTo))) dowork = true; break;
			}
			if (dowork) {
				mat[xTo, yTo] = new Dictionary<string, string>(mat[x, y]);
				mat[x, y].Clear();
				mat[x, y]["Who"] = "";
				mat[x, y]["Colour"] = "";
			}
			UpdateGrid();
		}
	}
}

